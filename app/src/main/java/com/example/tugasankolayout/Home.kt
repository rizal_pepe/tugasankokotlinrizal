package com.example.tugasankolayout

import android.graphics.Color
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.Gravity
import android.widget.LinearLayout
import android.widget.TextView
import org.jetbrains.anko.*

class Home : AppCompatActivity(), AnkoLogger {
    override fun onCreate(savedInstanceState: Bundle?) {
        ViewHome().setContentView(this)
        super.onCreate(savedInstanceState)
        val text = findViewById<TextView>(R.id.text)
        text.text = intent.getStringExtra("nama")
        val lat1 = findViewById<LinearLayout>(R.id.latihan1)
        val lat2 = findViewById<LinearLayout>(R.id.latihan2)
        val lat3 = findViewById<LinearLayout>(R.id.latihan3)
        lat1.setOnClickListener {
            toast("Latihan 1 Anko")
        }
        lat2.setOnClickListener {
            toast("Latihan 2 Anko")
        }
        lat3.setOnClickListener {
            toast("Latihan 3 Anko")
        }
    }

    override fun onBackPressed() {
        alert(title = "Warning!", message = "Apakah Anda Yakin") {
            positiveButton("OK") {
                finish()
            }
            negativeButton("CANCEL") {
                //
            }
        }.show()
    }

    class ViewHome : AnkoComponent<Home> {
        override fun createView(ui: AnkoContext<Home>) = with(ui) {
            verticalLayout {
                lparams {
                    width = matchParent
                    height = matchParent
                    backgroundColor = Color.WHITE
                    margin = 20
                }
                verticalLayout {
                    lparams(width = matchParent, height = wrapContent)
                    textView {
                        text = "Welcome"
                        textSize = 16.0f
                        textColor = Color.BLACK
                    }.lparams(width = matchParent, height = wrapContent) {
                        setMargins(50, 50, 50, 50)
                    }
                    textView {
                        id = R.id.text
                        textSize = 24.0f
                        isAllCaps = true
                        textColor = Color.BLACK
                    }.lparams(width = matchParent, height = wrapContent) {
                        setMargins(50, 0, 50, 0)
                    }
                    linearLayout {
                        lparams {
                            width = matchParent
                            height = wrapContent
                            id = R.id.latihan1
                            setMargins(50, 100, 50, 50)
                            backgroundColor = Color.GRAY
                        }
                        imageView {
                            imageResource = R.drawable.kotlin
                        }.lparams(width = 150, height = 150)
                        textView("Latihan Anko 1") {
                            textSize = 20.0f
                            textColor = Color.rgb(0, 0, 0)
                            textAlignment = TextView.TEXT_ALIGNMENT_CENTER
                        }.lparams(width = matchParent, height = wrapContent) {
                            gravity = Gravity.CENTER
                        }
                    }
                    linearLayout {
                        lparams {
                            width = matchParent
                            height = wrapContent
                            id = R.id.latihan2
                            setMargins(50, 50, 50, 50)
                            backgroundColor = Color.GRAY
                        }
                        imageView {
                            imageResource = R.drawable.kotlin
                        }.lparams(width = 150, height = 150)
                        textView("Latihan Anko 2") {
                            textSize = 20.0f
                            textColor = Color.rgb(0, 0, 0)
                            textAlignment = TextView.TEXT_ALIGNMENT_CENTER
                        }.lparams(width = matchParent, height = wrapContent) {
                            gravity = Gravity.CENTER
                        }
                    }
                    linearLayout {
                        lparams {
                            width = matchParent
                            height = wrapContent
                            id = R.id.latihan3
                            setMargins(50, 50, 50, 50)
                            backgroundColor = Color.GRAY
                        }
                        imageView {
                            imageResource = R.drawable.kotlin
                        }.lparams(width = 150, height = 150)
                        textView("Latihan Anko 3") {
                            textSize = 20.0f
                            textColor = Color.rgb(0, 0, 0)
                            textAlignment = TextView.TEXT_ALIGNMENT_CENTER
                        }.lparams(width = matchParent, height = wrapContent) {
                            gravity = Gravity.CENTER
                        }
                    }
                }
            }
        }
    }
}