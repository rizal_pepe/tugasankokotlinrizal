package com.example.tugasankolayout

import android.graphics.Color
import android.os.Bundle
import android.os.Handler
import android.support.v7.app.AppCompatActivity
import android.view.Gravity
import android.widget.TextView
import org.jetbrains.anko.*

class Splash : AppCompatActivity(), AnkoLogger {
    override fun onCreate(savedInstanceState: Bundle?) {
        ViewSplash().setContentView(this)
        super.onCreate(savedInstanceState)
        Handler().postDelayed({
            //start main activity
            startActivity(intentFor<Hello>())
            //finish this activity
            finish()
        }, 4000)
    }

    class ViewSplash : AnkoComponent<Splash> {
        override fun createView(ui: AnkoContext<Splash>) = with(ui) {
            verticalLayout {
                lparams {
                    width = matchParent
                    height = matchParent
                    backgroundColor = Color.LTGRAY
                    verticalGravity = Gravity.CENTER
                    horizontalGravity = Gravity.CENTER
                    margin = 20
                }
                imageView {
                    imageResource = R.drawable.anko
                }.lparams(width = 750, height = 400)
                textView {
                    text = "Mari Belajar Anko"
                    textSize = 20.0f
                    textColor = Color.rgb(51, 51, 51)
                    textAlignment = TextView.TEXT_ALIGNMENT_CENTER
                }.lparams(width = matchParent, height = wrapContent)
                textView {
                    text = "by Rizal Pangestu"
                    textSize = 16.0f
                    textColor = Color.rgb(51, 51, 51)
                    textAlignment = TextView.TEXT_ALIGNMENT_CENTER
                }.lparams(width = matchParent, height = wrapContent)
            }
        }

    }
}