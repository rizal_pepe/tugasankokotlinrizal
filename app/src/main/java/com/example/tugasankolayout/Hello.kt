package com.example.tugasankolayout

import android.graphics.Color
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.text.InputType
import android.view.Gravity
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import org.jetbrains.anko.*

class Hello : AppCompatActivity(), AnkoLogger {
    override fun onCreate(savedInstanceState: Bundle?) {
        ViewHello().setContentView(this)
        super.onCreate(savedInstanceState)
        val name = findViewById<EditText>(R.id.etName)
        val btn = findViewById<Button>(R.id.btn)
        btn.setOnClickListener {
            val names = name.text.toString().trim()
            startActivity(intentFor<Home>("nama" to names))
            finish()
        }
    }

    class ViewHello : AnkoComponent<Hello> {
        override fun createView(ui: AnkoContext<Hello>) = with(ui) {
            verticalLayout {
                lparams {
                    width = matchParent
                    height = matchParent
                    backgroundColor = Color.LTGRAY
                    margin = 20
                    padding = 20
                }
                imageView {
                    imageResource = R.drawable.anko
                }.lparams(width = 750, height = 400)
                verticalLayout {
                    lparams(width = matchParent, height = wrapContent) {
                        margin = 15
                        padding = 20
                        backgroundResource = R.drawable.bghello
                    }
                    textView {
                        text = "HELLO"
                        textSize = 24.0f
                        text
                        textColor = Color.rgb(0, 0, 0)
                        textAlignment = TextView.TEXT_ALIGNMENT_CENTER
                    }.lparams(width = matchParent, height = wrapContent) {
                        setMargins(20, 50, 20, 0)
                    }
                    editText {
                        hint = "Input Your Name"
                        textAlignment = TextView.TEXT_ALIGNMENT_CENTER
                        setHintTextColor(Color.GRAY)
                        textColor = Color.BLACK
                        inputType = InputType.TYPE_CLASS_TEXT
                        id = R.id.etName
                        backgroundResource = R.drawable.edittext
                    }.lparams(width = matchParent, height = wrapContent) {
                        setMargins(20, 50, 20, 0)
                    }
                    button("GO") {
                        backgroundResource = R.drawable.button
                        textColor = Color.rgb(255, 255, 255)
                        textSize = 16.0f
                        id = R.id.btn
                    }.lparams(width = 400, height = 100) {
                        setMargins(20, 50, 20, 50)
                        horizontalGravity = Gravity.CENTER
                    }
                }
            }
        }

    }
}